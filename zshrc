# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="ys"

export UPDATE_ZSH_DAYS=21

plugins=(git ruby rails)

# User configuration

export PATH=$HOME/bin:/usr/local/bin:$PATH

source $ZSH/oh-my-zsh.sh

export EDITOR='vim'

# Compilation flags
export ARCHFLAGS="-arch x86_64"

#Pacman aliases #TODO
alias pacin="sudo pacman -S"
alias pacrem="sudo pacman -Rns"

PATH="`ruby -e 'print Gem.user_dir'`/bin:$PATH"

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
